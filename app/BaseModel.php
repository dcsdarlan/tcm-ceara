<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 12/01/17
 * Time: 10:25
 */
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {

    public $timestamps = false;
    protected $dFormat = 'd/m/Y';
    protected $dtFormat = 'd/m/Y H:i:s';
    protected function formatNullFromString($attr) {
        return ($attr) ? $attr : "";
    }
    protected function formatDateFromString($attr) {
        return Carbon::parse($attr)->format($this->dFormat);
    }
    protected function formatDateTimeFromString($attr) {
        return Carbon::parse($attr)->format($this->dtFormat);
    }
}