<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class LicitacaoObjeto extends BaseModel {
    protected $table = 'licitacao_objeto';
    protected $fillable = [
        'id',
        'id_licitacao',
        'objeto'
    ];
}