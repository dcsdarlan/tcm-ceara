<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class LicitacaoLicitante extends BaseModel {
    protected $table = 'licitacao_licitante';
    protected $fillable = [
        'id',
        'id_licitacao',
        'id_objeto',
        'licitante',
        'documento',
        'valor'
    ];
}