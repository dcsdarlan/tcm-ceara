<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class LicitacaoPublicacao extends BaseModel {
    protected $table = 'licitacao_publicacao';
    protected $fillable = [
        'id',
        'id_licitacao',
        'categoria',
        'especificacao',
        'data'
    ];
    public function getDataAttribute($attr) {
        return $this->formatDateFromString($attr);
    }
}