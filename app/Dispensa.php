<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 11/11/16
 * Time: 10:01
 */

namespace App;


class Dispensa extends BaseModel {
    protected $table = 'dispensa_inexigibilidade';
    protected $fillable = [
        'id',
        'dispensa_inexigibilidade',
        'codigo_cidade',
        'data_abertura',
        'objetivo',
        'situacao',
        'link',
        'exercicio',
        'processo_administrativo',
        'fundamentacao_legal',
        'ordenador_despesa',
        'responsavel_dispensa',
        'responsavel_informacao',
        'tipo_responsavel_informacao'
    ];

    public function getDataAberturaAttribute($attr) {
        return $this->formatDateFromString($attr);
    }
    public function publicacoes() {
        return $this->hasMany('App\DispensaPublicacao', 'id_licitacao', 'id');
    }
    public function objetos() {
        return $this->hasMany('App\DispensaObjeto', 'id_licitacao', 'id');
    }
    public function orgaos() {
        return $this->hasMany('App\DispensaOrgao', 'id_licitacao', 'id');
    }
    public function licitantes() {
        return $this->hasMany('App\DispensaLicitante', 'id_licitacao', 'id');
    }
    public static function abertas($cod) {
        return Dispensa::where("situacao", "=", "aberta")
            ->where("codigo_cidade", "=", $cod)
            ->with("publicacoes")
            ->with("objetos")
            ->with("orgaos")
            ->with("licitantes")
            ->get();
    }
    public static function concluidas($cod) {
        return Dispensa::where("situacao", "=", "concluida")
            ->where("codigo_cidade", "=", $cod)
            ->with("publicacoes")
            ->with("objetos")
            ->with("orgaos")
            ->with("licitantes")
            ->get();
    }
    public static function findByCode($cod) {
        return Dispensa::where("dispensa_inexigibilidade", "=", "$cod")
            ->first();
    }
}