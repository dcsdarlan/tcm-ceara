<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class RegistroPrecoObjeto extends BaseModel {
    protected $table = 'registro_preco_objeto';
    protected $fillable = [
        'id',
        'id_registro_preco',
        'objeto'
    ];
}