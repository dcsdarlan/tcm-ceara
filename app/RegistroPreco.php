<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 11/11/16
 * Time: 10:01
 */

namespace App;


class RegistroPreco extends BaseModel {
    protected $table = 'registro_preco';
    protected $fillable = [
        'id',
        'registro_preco',
        'codigo_cidade',
        'data_abertura',
        'objetivo',
        'situacao',
        'link',
        'exercicio',
        'processo_administrativo',
        'fundamentacao_legal',
        'ordenador_despesa',
        'responsavel_registro_preco',
        'responsavel_informacao',
        'tipo_responsavel_informacao'
    ];

    public function getDataAberturaAttribute($attr) {
        return $this->formatDateFromString($attr);
    }
    public function publicacoes() {
        return $this->hasMany('App\RegistroPrecoPublicacao', 'id_licitacao', 'id');
    }
    public function objetos() {
        return $this->hasMany('App\RegistroPrecoObjeto', 'id_licitacao', 'id');
    }
    public function orgaos() {
        return $this->hasMany('App\RegistroPrecoOrgao', 'id_licitacao', 'id');
    }
    public function licitantes() {
        return $this->hasMany('App\RegistroPrecoLicitante', 'id_licitacao', 'id');
    }

    public static function abertas($cod) {
        return RegistroPreco::where("situacao", "=", "aberta")
            ->where("codigo_cidade", "=", $cod)
            ->with("publicacoes")
            ->with("objetos")
            ->with("orgaos")
            ->with("licitantes")
            ->get();
    }
    public static function concluidas($cod) {
        return RegistroPreco::where("situacao", "=", "concluida")
            ->where("codigo_cidade", "=", $cod)
            ->with("publicacoes")
            ->with("objetos")
            ->with("orgaos")
            ->with("licitantes")
            ->get();
    }
    public static function findByCode($cod) {
        return RegistroPreco::where("registro_preco", "=", "$cod")
            ->first();
    }
}