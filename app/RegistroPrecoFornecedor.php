<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class RegistroPrecoFornecedor extends BaseModel {
    protected $table = 'registro_preco_fornecedor';
    protected $fillable = [
        'id',
        'id_registro_preco',
        'id_objeto',
        'fornecedor',
        'documento',
        'valor'
    ];
}