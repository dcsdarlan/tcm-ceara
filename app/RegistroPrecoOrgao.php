<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class RegistroPrecoOrgao extends BaseModel {
    protected $table = 'registro_preco_orgao';
    protected $fillable = [
        'id',
        'id_registro_preco',
        'orgao'
    ];
}