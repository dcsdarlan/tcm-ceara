<?php

namespace App\Http\Controllers;

use App\Licitacao;
use Illuminate\Http\Request;
use DateTime;

class LicitacaoController extends BaseController {
    public function abertas(Request $request, $codigoCidade) {
        return response()->json(Licitacao::abertas($codigoCidade), 200, [], JSON_UNESCAPED_UNICODE);
    }
    public function concluidas(Request $request, $codigoCidade) {
        return response()->json(Licitacao::concluidas($codigoCidade), 200, [], JSON_UNESCAPED_UNICODE);
    }
    public function detalhar(Request $request, $cidade, $x, $y = false) {
        if($y) {
            $codigo = $x . "/" . $y;
        } else {
            $codigo = $x;
        }
        return response()->json(Licitacao::findByCode($cidade, $codigo), 200, [], JSON_UNESCAPED_UNICODE);
    }
    public function filtrar(Request $request, $codigoCidade) {
        return response()->json(Licitacao::filtrar($codigoCidade,
                ($request->codigo ? $request->codigo : null),
                ($request->objeto ? $request->objeto : null),
                ($request->modalidade ? $request->modalidade : null),
                ($request->data ? DateTime::createFromFormat('d/m/Y', $request->data)->format('Y-m-d') : null)
            ), 200, [], JSON_UNESCAPED_UNICODE);
    }
}
