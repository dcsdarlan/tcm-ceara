<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class BaseController extends Controller {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static $cidades = [
        "crateus" => "048",
        "ico" => "073",
        "eusebio" => "054",
        "maranguape" => "100",
        "pacajus" => "122",
        "pedra_branca" => "132"
    ];
}
