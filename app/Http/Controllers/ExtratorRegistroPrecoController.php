<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 12/01/17
 * Time: 10:12
 */

namespace App\Http\Controllers;


use App\Classes\TCM\TCMCearaLicitacao;
use App\Classes\TCM\TCMCearaRegistroPreco;
use App\Licitacao;

class ExtratorRegistroPrecoController extends BaseController {

    public function extrair($codigoCidade) {
        $tcm = new  TCMCearaRegistroPreco($codigoCidade);
        $tcm->extrair();
        //$tcm->limparBase();
    }

    public function crateus() {
        $codigoCidade = self::$cidades["crateus"];
        $tcm = new  TCMCearaRegistroPreco($codigoCidade);
        $tcm->extrair();
        //$tcm->limparBase();
    }

    public function ico() {
        $codigoCidade = self::$cidades["ico"];
        $tcm = new  TCMCearaRegistroPreco($codigoCidade);
        $tcm->extrair();
        //$tcm->limparBase();
    }

    public function eusebio() {
        $codigoCidade = self::$cidades["eusebio"];
        $tcm = new  TCMCearaRegistroPreco($codigoCidade);
        $tcm->extrair();
        //$tcm->limparBase();
    }

    public function maranguape() {
        $codigoCidade = self::$cidades["maranguape"];
        $tcm = new  TCMCearaRegistroPreco($codigoCidade);
        $tcm->extrair();
        //$tcm->limparBase();
    }
    public function pacajus() {
        $codigoCidade = self::$cidades["pacajus"];
        $tcm = new  TCMCearaRegistroPreco($codigoCidade);
        $tcm->extrair();
        //$tcm->limparBase();
    }

    public function pedrabranca() {
        $codigoCidade = self::$cidades["pedra_branca"];
        $tcm = new  TCMCearaRegistroPreco($codigoCidade);
        $tcm->extrair();
        //$tcm->limparBase();
    }

}