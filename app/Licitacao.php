<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 11/11/16
 * Time: 10:01
 */

namespace App;


class Licitacao extends BaseModel {
    protected $table = 'licitacao';
    protected $fillable = [
        'id',
        'licitacao',
        'codigo_cidade',
        'objetivo',
        'data_abertura',
        'link',
        'exercicio',
        'modalidade',
        'tipo',
        'situacao',
        'data_aviso',
        'hora_abertura',
        'local',
        'processo_administrativo',
        'fundamentacao_legal',
        'ordenador_despesa',
        'pregoeiro_presidente_comissao',
        'responsavel_informacao',
        'responsavel_parecer_tecnico_juridico',
        'responsavel_adjudicacao',
        'responsavel_homologacao'
    ];
    public function getDataAberturaAttribute($attr) {
        return $this->formatDateFromString($attr);
    }
    public function getDataAvisoAttribute($attr) {
        return $this->formatDateFromString($attr);
    }
    public function publicacoes() {
        return $this->hasMany('App\LicitacaoPublicacao', 'id_licitacao', 'id');
    }
    public function objetos() {
        return $this->hasMany('App\LicitacaoObjeto', 'id_licitacao', 'id');
    }
    public function orgaos() {
        return $this->hasMany('App\LicitacaoOrgao', 'id_licitacao', 'id');
    }
    public function licitantes() {
        return $this->hasMany('App\LicitacaoLicitante', 'id_licitacao', 'id');
    }
    public static function abertas($cod) {
        return Licitacao::where("situacao", "=", "aberta")
            ->where("codigo_cidade", "=", $cod)
            ->with("publicacoes")
            ->with("objetos")
            ->with("orgaos")
            ->with("licitantes")
            ->get();
    }
    public static function concluidas($cod) {
        return Licitacao::whereIn("situacao", ["concluida", "finalizada"])
            ->where("codigo_cidade", "=", $cod)
            ->with("publicacoes")
            ->with("objetos")
            ->with("orgaos")
            ->with("licitantes")
            ->get();
    }
    public static function findByCode($cidade, $cod) {
        return Licitacao::where("licitacao", "=", "$cod")
            ->where("codigo_cidade", "=", $cidade)
            ->with("publicacoes")
            ->with("objetos")
            ->with("orgaos")
            ->with("licitantes")
            ->first();
    }
    public static function filtrar($codCidade, $codLicitacao, $objLicitacao, $modLicitacao, $dtLicitacao ) {
        $licitacoes = Licitacao::where("codigo_cidade", "=", $codCidade);
        if($codLicitacao) {
            $codLicitacao->where("licitacao", "like", "%$codLicitacao%");
        }
        if($objLicitacao) {
            $codLicitacao->where("objetivo", "like", "%$objLicitacao%");
        }
        if($modLicitacao) {
            $codLicitacao->where("modalidade", "like", "%$modLicitacao%");
        }
        if($dtLicitacao) {
            $codLicitacao->where("data_abertura", "=", $dtLicitacao);
        }
        return $licitacoes->get();
    }
}