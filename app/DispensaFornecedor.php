<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class DispensaFornecedor extends BaseModel {
    protected $table = 'dispensa_inexigibilidade_fornecedor';
    protected $fillable = [
        'id',
        'id_dispensa_inexigibilidade',
        'id_objeto',
        'fornecedor',
        'documento',
        'valor'
    ];
}