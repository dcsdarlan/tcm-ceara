<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class DispensaOrgao extends BaseModel {
    protected $table = 'dispensa_inexigibilidade_orgao';
    protected $fillable = [
        'id',
        'id_dispensa_inexigibilidade',
        'orgao'
    ];
}