<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class LicitacaoOrgao extends BaseModel {
    protected $table = 'licitacao_orgao';
    protected $fillable = [
        'id',
        'id_licitacao',
        'orgao'
    ];
}