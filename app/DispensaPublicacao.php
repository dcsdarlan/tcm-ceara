<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class DispensaPublicacao extends BaseModel {
    protected $table = 'dispensa_inexigibilidade_publicacao';
    protected $fillable = [
        'id',
        'id_dispensa_inexigibilidade',
        'categoria',
        'especificacao',
        'data'
    ];
    public function getDataAttribute($attr) {
        return $this->formatDateFromString($attr);
    }
}