<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 14/11/16
 * Time: 15:55
 */

namespace App;


class DispensaObjeto extends BaseModel {
    protected $table = 'dispensa_inexigibilidade_objeto';
    protected $fillable = [
        'id',
        'id_dispensa_inexigibilidade',
        'objeto'
    ];
}