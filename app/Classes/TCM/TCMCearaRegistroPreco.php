<?php
namespace App\Classes\TCM;

use App\Dispensa;
use App\DispensaFornecedor;
use App\DispensaObjeto;
use App\DispensaOrgao;
use App\DispensaPublicacao;
use App\Licitacao;
use App\LicitacaoLicitante;
use App\LicitacaoObjeto;
use App\LicitacaoOrgao;
use App\LicitacaoPublicacao;
use App\RegistroPreco;
use App\RegistroPrecoFornecedor;
use App\RegistroPrecoObjeto;
use App\RegistroPrecoOrgao;
use App\RegistroPrecoPublicacao;
use Sunra\PhpSimple\HtmlDomParser;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;

class TCMCearaRegistroPreco extends TCMCeara {

    function __construct($codigoCidade) {
        parent::__construct($codigoCidade);
    }

    public function extrairRegistroPrecoDetalhes($rp) {
        $url = self::URL_TCM . $rp->link;
        $html = HtmlDomParser::file_get_html($url);
        $this->extrairRegistroPreco($rp, $html);
    }

    public function extrairRegistroPreco($rp, $html) {
        //Detalhes
        echo "Detalhes<br>\n";
        $detalhes = array();
        foreach (explode("<br />", $html->find('.well', 0)) as $it) {
            $dt = explode(":", trim($it));
            if(count($dt) > 1) {
                $detalhes[trim(strip_tags($dt[0]))] = trim(strip_tags($dt[1]));
            }
        }
        print_r($detalhes);
        $rp->exercicio = $detalhes["Exercício"];
        echo "<br>\n";


        //Dados Complementares
        echo "Dados Complementares<br>\n";
        $complemento = array();

        foreach (explode("<br />", $html->find('.well', 4)) as $it) {
            $comp = explode(":", trim(strip_tags($it)));
            if(count($comp) > 1) {
                if(trim(strip_tags($comp[0])) == "Nº do Processo Administrativo") {
                    $np = explode("|", trim($comp[1]));
                    $complemento["Nº do Processo Administrativo"] = trim($np[0]);
                    $complemento["Fundamentação Legal"] = trim($comp[2]);

                } else {
                    $complemento[trim(strip_tags($comp[0]))] = trim(strip_tags($comp[1]));
                }
            }
        }
        print_r($complemento);
        if(array_key_exists("Nº do Processo Administrativo", $complemento))$rp->processo_administrativo = $complemento["Nº do Processo Administrativo"];
        if(array_key_exists("Fundamentação Legal", $complemento))$rp->fundamentacao_legal = $complemento["Fundamentação Legal"];
        if(array_key_exists("Ordenador da Despesa", $complemento))$rp->ordenador_despesa = $complemento["Ordenador da Despesa"];
        if(array_key_exists("Responsável pela Adesão de Registro de Preço", $complemento))$rp->responsavel_registro_preco = $complemento["Responsável pela Adesão de Registro de Preço"];
        if(array_key_exists("Responsável pela Informação", $complemento))$rp->responsavel_informacao = $complemento["Responsável pela Informação"];
        if(array_key_exists("Tipo de Responsável pela Informação", $complemento))$rp->tipo_responsavel_informacao = $complemento["Tipo de Responsável pela Informação"];

        //Verificar se é Camara municipal
        $isCamara = false;
        $stringsCamara = [
            "Camara Municipal",
            "CAMARA MUNICIPAL"
        ];
        foreach ($html->find('.well', 2)->find("ul", 0)->find("li") as $it) {
            foreach ($stringsCamara as $stCam) {
                if(strpos( $it->plaintext, $stCam) !== false) {
                    $isCamara = true;
                }
            }
        }
        if(!$isCamara) {
            try {
                $rp->save();
                echo "Salvando: " . $rp->registro_preco;
                echo "<br>\n";
                $this->extrairPublicacao($rp, $html->find('.well', 1));
                $this->extrairOrgao($rp, $html->find('.well', 2));
                $this->extrairFornecedor($rp, $html->find('.well', 3));
            } catch (Exception $e) {
                throw $e;
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        } else {
            try {
                echo "Apagando: " . $rp->registro_preco;
                echo "Motivo: Camara Municipal";
                $rp->delete();
                echo "<br>\n";
            } catch (Exception $e) {
                throw $e;
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
    }

    public function extrairPublicacao($rp, $html) {
        //Publicacao
        echo "Publicações<br>\n";
        $publicacoes = array();
        foreach ($html->find("ul", 0)->find("li") as $it) {
            $pb = explode("|", trim($it->plaintext));
            if(count($pb) > 2) {
                $pub = array();
                $pub["Categoria"] = trim($pb[0]);
                $esp = explode(":", trim($pb[1]));
                $pub["Especificação"] = trim($esp[1]);
                $dt = explode(":", trim($pb[2]));
                $pub["Data"] = trim($dt[1]);
                $publicacoes[] = $pub;
            }
        }
        print_r($publicacoes);
        foreach ($publicacoes as $pbl) {
            try {
                $publicacao = RegistroPrecoPublicacao::where("id_registro_preco", "=", $rp->id)->where("especificacao", "=", $pbl["Especificação"])->first();
                if($publicacao) {
                    $publicacao->id_registro_preco = $rp->id;
                    $publicacao->especificacao = $pbl["Especificação"];
                    $publicacao->categoria = $pbl["Categoria"];
                    $data = DateTime::createFromFormat('d-m-Y', $pbl["Data"]);
                    $publicacao->data = $data->format('Y-m-d');
                    $publicacao->save();
                } else {
                    $publicacao = new RegistroPrecoPublicacao();
                    $publicacao->id_registro_preco = $rp->id;
                    $publicacao->especificacao = $pbl["Especificação"];
                    $publicacao->categoria = $pbl["Categoria"];
                    $data = DateTime::createFromFormat('d-m-Y', $pbl["Data"]);
                    $publicacao->data = $data->format('Y-m-d');
                    $publicacao->save();
                }
            } catch (Exception $e) {
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
        echo "<br>\n";
    }

    public function extrairOrgao($rp, $html) {
        //Publicacao
        echo "Orgãos<br>\n";
        $orgaos = array();

        foreach ($html->find("ul", 0)->find("li") as $it) {
            $orgaos[] = $it->plaintext;
        }
        print_r($orgaos);
        foreach ($orgaos as $og) {
            try {
                $orgao = RegistroPrecoOrgao::where("id_registro_preco", "=", $rp->id)->where("orgao", "=", $og)->first();
                if(!$orgao) {
                    $orgao = new RegistroPrecoOrgao();
                    $orgao->id_registro_preco = $rp->id;
                    $orgao->orgao = $og;
                    $orgao->save();
                }
            } catch (Exception $e) {
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
        echo "<br>\n";
    }
    public function extrairFornecedor($rp, $html) {
        //Publicacao
        echo "Fornecedores<br>\n";
        $fornecedores = array();
        foreach ($html->find("ul", 0)->find("li") as $it) {
            if($it->class == "vencedor") {
                $forn = array();
                $fArray = explode("|", trim($it->plaintext));
                print_r($fArray);
                $forn["fornecedor"] = (explode(":", $fArray[0]))[1];
                $forn["documento"] = (explode(":", $fArray[1]))[1];
                $obArray = explode(":", $fArray[2]);
                array_shift($obArray);
                $forn["objeto"] = implode(":", $obArray);
                $forn["valor"] = (int)str_replace(",", ".", str_replace(".", "", str_replace("R$ ", "",  (explode(":", $fArray[count($fArray) - 1]))[1])));
                $fornecedores[] = $forn;
            }
        }
        print_r($fornecedores);
        foreach ($fornecedores as $frn) {
            try {
                $fornecedor = RegistroPrecoFornecedor::where("id_registro_preco", "=", $rp->id)->where("fornecedor", "=", $frn["fornecedor"])->first();
                if(!$fornecedor) {
                    $objeto = RegistroPrecoObjeto::where("id_registro_preco", "=", $rp->id)->where("objeto", "=", $frn["objeto"])->first();
                    if(!$objeto) {
                        $objeto = new RegistroPrecoObjeto();
                        $objeto->id_registro_preco = $rp->id;
                        $objeto->objeto = $frn["objeto"];
                        $objeto->save();
                    }
                    $fornecedor = new RegistroPrecoFornecedor();
                    $fornecedor->id_registro_preco = $rp->id;
                    $fornecedor->documento = $frn["documento"];
                    $fornecedor->id_objeto = $objeto->id;
                    $fornecedor->fornecedor = $frn["fornecedor"];
                    $fornecedor->valor = $frn["valor"];
                    $fornecedor->save();
                }
            } catch (Exception $e) {
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
        echo "<br>\n";
    }


    public function extrair() {
        $licCont = array();
        $contiue = true;
        $page = 1;
        while($contiue) {
            $url = self::URL_REGISTRO_PRECO . "/abertas/page/" . $page . "/mun/" . $this->codigoCidade;
            $html = HtmlDomParser::file_get_html($url);
            echo "URL:" . $url;
            echo "<br>";
            if($html->find('table', 0)->find("tr", 1)) {
                foreach ($html->find('table', 0)->find("tr") as $row) {
                    if ($row->find("td", 0)) {
                        $registro = RegistroPreco::where("registro_preco", "like", trim($row->find("a", 0)->plaintext))->where("codigo_cidade", "=", $this->codigoCidade)->first();
                        echo "Dispensa: " . trim($row->find("a", 0)->plaintext);
                        echo "<br>";
                        try {
                            if ($registro) {
                                $registro->codigo_cidade = $this->codigoCidade;
                                $registro->link = trim($row->find("a", 0)->href);
                                $registro->registro_preco = trim($row->find("a", 0)->plaintext);
                                $registro->objetivo = utf8_decode(utf8_encode(trim($row->find("td", 2)->plaintext)));
                                $date = DateTime::createFromFormat('d/m/Y', trim($row->find("td", 3)->plaintext));
                                $registro->data_abertura = $date->format('Y-m-d');

                                if(strripos(trim($row->find("td", 4)->plaintext), "Fechada") !== false) $registro->situacao = "fechada";
                                if(strripos(trim($row->find("td", 4)->plaintext), "Aberta") !== false) $registro->situacao = "aberta";
                                if(strripos(trim($row->find("td", 4)->plaintext), "Concluida") !== false) $registro->situacao = "concluida";
                                if(strripos(trim($row->find("td", 4)->plaintext), "Suspensa") !== false) $registro->situacao = "suspensa";
                                if(strripos(trim($row->find("td", 4)->plaintext), "Finalizada") !== false) $registro->situacao = "finalizada";

                                $registro->save();
                                $this->extrairRegistroPrecoDetalhes($registro);
                                echo "ALTERADO<br>";
                            } else {
                                $registro = new RegistroPreco();
                                $registro->codigo_cidade = $this->codigoCidade;
                                $registro->link = trim($row->find("a", 0)->href);
                                $registro->registro_preco = trim($row->find("a", 0)->plaintext);
                                $registro->objetivo = utf8_decode(utf8_encode(trim($row->find("td", 2)->plaintext)));
                                $date = DateTime::createFromFormat('d/m/Y', trim($row->find("td", 3)->plaintext));
                                $registro->data_abertura = $date->format('Y-m-d');

                                if(strripos(trim($row->find("td", 4)->plaintext), "Fechada") !== false) $registro->situacao = "fechada";
                                if(strripos(trim($row->find("td", 4)->plaintext), "Aberta") !== false) $registro->situacao = "aberta";
                                if(strripos(trim($row->find("td", 4)->plaintext), "Concluida") !== false) $registro->situacao = "concluida";
                                if(strripos(trim($row->find("td", 4)->plaintext), "Suspensa") !== false) $registro->situacao = "suspensa";
                                if(strripos(trim($row->find("td", 4)->plaintext), "Finalizada") !== false) $registro->situacao = "finalizada";

                                if(((int)$date->format("Y")) > (((int)date("Y")) - 2)) {
                                    $registro->save();
                                    $this->extrairRegistroPrecoDetalhes($registro);
                                    echo "CRIADO<br>";
                                } else {
                                    echo "Ano: " . (int)$date->format("Y");
                                    echo "IGNORADO<br>";
                                }
                            }


                        } catch (Exception $e) {
                            throw $e;
                            echo "Erro: " . $e->getMessage();
                            echo "<br>";
                        }
                        if (trim($row->find("a", 0)->plaintext) === "" || in_array(trim($row->find("a", 0)->plaintext), $licCont)) {
                            echo "ENCERROU";
                            $contiue = false;
                        } else {
                            echo "CONTINUA";
                            $licCont[] = trim($row->find("a", 0)->plaintext);
                            $contiue = true;
                        }
                        echo "<br>";
                    }
                }
                $page++;
                sleep(1);
            } else {
                echo "ENCERROU - SEM RESULTADO";
                $contiue = false;
                echo "<br>";
            }
        }
    }


    public function limparBase(){
        DB::table('dispensa_inexigibilidade')->truncate();
        DB::table('dispensa_inexigibilidade_publicacao')->truncate();
        DB::table('dispensa_inexigibilidade_orgao')->truncate();
        DB::table('dispensa_inexigibilidade_objeto')->truncate();
        DB::table('dispensa_inexigibilidade_fornecedor')->truncate();
        return "Limpeza realizada com sucesso!";
    }

    function win_to_utf8($str) {
        $f[]="\xc2\xac";  $t[]="\x80";
        $f[]="\xd9\xbe";  $t[]="\x81";
        $f[]="\xc0\x9a";  $t[]="\x82";
        $f[]="\xc6\x92";  $t[]="\x83";
        $f[]="\xc0\x9e";  $t[]="\x84";
        $f[]="\xc0\xa6";  $t[]="\x85";
        $f[]="\xc0\xa0";  $t[]="\x86";
        $f[]="\xc0\xa1";  $t[]="\x87";
        $f[]="\xcb\x86";  $t[]="\x88";
        $f[]="\xc0\xb0";  $t[]="\x89";
        $f[]="\xd9\xb9";  $t[]="\x8a";
        $f[]="\xc0\xb9";  $t[]="\x8b";
        $f[]="\xc5\x92";  $t[]="\x8c";
        $f[]="\xda\x86";  $t[]="\x8d";
        $f[]="\xda\x98";  $t[]="\x8e";
        $f[]="\xda\x88";  $t[]="\x8f";
        $f[]="\xda\xaf";  $t[]="\x90";
        $f[]="\xc0\x98";  $t[]="\x91";
        $f[]="\xc0\x99";  $t[]="\x92";
        $f[]="\xc0\x9c";  $t[]="\x93";
        $f[]="\xc0\x9d";  $t[]="\x94";
        $f[]="\xc0\xa2";  $t[]="\x95";
        $f[]="\xc0\x93";  $t[]="\x96";
        $f[]="\xc0\x94";  $t[]="\x97";
        $f[]="\xda\xa9";  $t[]="\x98";
        $f[]="\xc4\xa2";  $t[]="\x99";
        $f[]="\xda\x91";  $t[]="\x9a";
        $f[]="\xc0\xba";  $t[]="\x9b";
        $f[]="\xc5\x93";  $t[]="\x9c";
        $f[]="\xc0\x8c";  $t[]="\x9d";
        $f[]="\xc0\x8d";  $t[]="\x9e";
        $f[]="\xda\xba";  $t[]="\x9f";
        $f[]="\xd8\x8c";  $t[]="\xa1";
        $f[]="\xda\xbe";  $t[]="\xaa";
        $f[]="\xd8\x9b";  $t[]="\xba";
        $f[]="\xd8\x9f";  $t[]="\xbf";
        $f[]="\xdb\x81";  $t[]="\xc0";
        $f[]="\xd8\xa1";  $t[]="\xc1";
        $f[]="\xd8\xa2";  $t[]="\xc2";
        $f[]="\xd8\xa3";  $t[]="\xc3";
        $f[]="\xd8\xa4";  $t[]="\xc4";
        $f[]="\xd8\xa5";  $t[]="\xc5";
        $f[]="\xd8\xa6";  $t[]="\xc6";
        $f[]="\xd8\xa7";  $t[]="\xc7";
        $f[]="\xd8\xa8";  $t[]="\xc8";
        $f[]="\xd8\xa9";  $t[]="\xc9";
        $f[]="\xd8\xaa";  $t[]="\xca";
        $f[]="\xd8\xab";  $t[]="\xcb";
        $f[]="\xd8\xac";  $t[]="\xcc";
        $f[]="\xd8\xad";  $t[]="\xcd";
        $f[]="\xd8\xae";  $t[]="\xce";
        $f[]="\xd8\xaf";  $t[]="\xcf";
        $f[]="\xd8\xb0";  $t[]="\xd0";
        $f[]="\xd8\xb1";  $t[]="\xd1";
        $f[]="\xd8\xb2";  $t[]="\xd2";
        $f[]="\xd8\xb3";  $t[]="\xd3";
        $f[]="\xd8\xb4";  $t[]="\xd4";
        $f[]="\xd8\xb5";  $t[]="\xd5";
        $f[]="\xd8\xb6";  $t[]="\xd6";
        $f[]="\xd8\xb7";  $t[]="\xd8";
        $f[]="\xd8\xb8";  $t[]="\xd9";
        $f[]="\xd8\xb9";  $t[]="\xda";
        $f[]="\xd8\xba";  $t[]="\xdb";
        $f[]="\xd9\x80";  $t[]="\xdc";
        $f[]="\xd9\x81";  $t[]="\xdd";
        $f[]="\xd9\x82";  $t[]="\xde";
        $f[]="\xd9\x83";  $t[]="\xdf";
        $f[]="\xd9\x84";  $t[]="\xe1";
        $f[]="\xd9\x85";  $t[]="\xe3";
        $f[]="\xd9\x86";  $t[]="\xe4";
        $f[]="\xd9\x87";  $t[]="\xe5";
        $f[]="\xd9\x88";  $t[]="\xe6";
        $f[]="\xd9\x89";  $t[]="\xec";
        $f[]="\xd9\x8a";  $t[]="\xed";
        $f[]="\xd9\x8b";  $t[]="\xf0";
        $f[]="\xd9\x8c";  $t[]="\xf1";
        $f[]="\xd9\x8d";  $t[]="\xf2";
        $f[]="\xd9\x8e";  $t[]="\xf3";
        $f[]="\xd9\x8f";  $t[]="\xf5";
        $f[]="\xd9\x90";  $t[]="\xf6";
        $f[]="\xd9\x91";  $t[]="\xf8";
        $f[]="\xd9\x92";  $t[]="\xfa";
        $f[]="\xc0\x8e";  $t[]="\xfd";
        $f[]="\xc0\x8f";  $t[]="\xfe";
        $f[]="\xdb\x92";  $t[]="\xff";
        return str_replace($t, $f, $str);
    }
    
}