<?php
namespace App\Classes\TCM;

use App\Licitacao;
use App\LicitacaoLicitante;
use App\LicitacaoObjeto;
use App\LicitacaoOrgao;
use App\LicitacaoPublicacao;
use Sunra\PhpSimple\HtmlDomParser;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;

class TCMCearaLicitacao extends TCMCeara {

    function __construct($codigoCidade) {
        parent::__construct($codigoCidade);
    }

    public function extrairAbertas() {
        $licCont = array();
        $contiue = true;
        $page = 1;
        while($contiue) {
            $url = self::URL_LICITACOES . "/abertas/page/" . $page . "/mun/" . $this->codigoCidade;
            $html = HtmlDomParser::file_get_html($url);
            echo "URL:" . $url;
            echo "<br>";
            if($html->find('table', 0)->find("tr", 1)) {
                foreach ($html->find('table', 0)->find("tr") as $row) {
                    if ($row->find("td", 0)) {
                        $licitacao = Licitacao::where("licitacao", "like", trim($row->find("a", 0)->plaintext))->where("codigo_cidade", "=", $this->codigoCidade)->first();
                        echo "Licitacao: " . trim($row->find("a", 0)->plaintext);
                        echo "<br>";
                        try {
                            if ($licitacao) {
                                $licitacao->codigo_cidade = $this->codigoCidade;
                                $licitacao->link = trim($row->find("a", 0)->href);
                                $licitacao->licitacao = trim($row->find("a", 0)->plaintext);
                                $licitacao->objetivo = utf8_decode(utf8_encode(trim($row->find("td", 2)->plaintext)));
                                $date = DateTime::createFromFormat('d/m/Y', trim($row->find("td", 3)->plaintext));
                                $licitacao->data_abertura = $date->format('Y-m-d');
                                $licitacao->situacao = "aberta";
                                $licitacao->save();
                                $this->extrairLicitacaoDetalhes($licitacao);
                                echo "ALTERADO<br>";
                            } else {
                                $licitacao = new Licitacao();
                                $licitacao->codigo_cidade = $this->codigoCidade;
                                $licitacao->link = trim($row->find("a", 0)->href);
                                $licitacao->licitacao = trim($row->find("a", 0)->plaintext);
                                $licitacao->objetivo = utf8_decode(utf8_encode(trim($row->find("td", 2)->plaintext)));
                                $date = DateTime::createFromFormat('d/m/Y', trim($row->find("td", 3)->plaintext));
                                $licitacao->data_abertura = $date->format('Y-m-d');
                                $licitacao->situacao = "aberta";
                                if(((int)$date->format("Y")) > (((int)date("Y")) - 2)) {
                                    $licitacao->save();
                                    $this->extrairLicitacaoDetalhes($licitacao);
                                    echo "CRIADO<br>";
                                } else {
                                    echo "Ano: " . (int)$date->format("Y");
                                    echo "IGNORADO<br>";
                                }
                            }

                        } catch (Exception $e) {
                            throw $e;
                            echo "Erro: " . $e->getMessage();
                            echo "<br>";
                        }
                        if (trim($row->find("a", 0)->plaintext) === "" || in_array(trim($row->find("a", 0)->plaintext), $licCont)) {
                            echo "ENCERROU";
                            $contiue = false;
                        } else {
                            echo "CONTINUA";
                            $licCont[] = trim($row->find("a", 0)->plaintext);
                            $contiue = true;
                        }
                        echo "<br>";
                    }
                }
                $page++;
                sleep(1);
            } else {
                echo "ENCERROU - SEM RESULTADO";
                $contiue = false;
                echo "<br>";
            }
        }
    }

    public function extrairConcluidas() {
        $licCont = array();
        $contiue = true;
        $page = 1;
        while($contiue) {
            $url = self::URL_LICITACOES . "/concluidas/page/" . $page . "/mun/" . $this->codigoCidade;
            $html = HtmlDomParser::file_get_html($url);
            echo "URL:" . $url;
            echo "<br>";
            if($html->find('table', 0)->find("tr", 1)) {
                foreach ($html->find('table', 0)->find("tr") as $row) {
                    if ($row->find("td", 0)) {
                        $licitacao = Licitacao::where("licitacao", "like", trim($row->find("a", 0)->plaintext))->where("codigo_cidade", "=", $this->codigoCidade)->first();
                        echo "Licitacao: " . trim($row->find("a", 0)->plaintext);
                        echo "<br>";
                        try {
                            if ($licitacao) {
                                $licitacao->codigo_cidade = $this->codigoCidade;
                                $licitacao->link = trim($row->find("a", 0)->href);
                                $licitacao->licitacao = trim($row->find("a", 0)->plaintext);
                                $licitacao->objetivo = utf8_decode(utf8_encode(trim($row->find("td", 2)->plaintext)));
                                $date = DateTime::createFromFormat('d/m/Y', trim($row->find("td", 3)->plaintext));
                                $licitacao->data_abertura = $date->format('Y-m-d');
                                $licitacao->situacao = "concluida";
                                $licitacao->save();
                                $this->extrairLicitacaoDetalhes($licitacao);
                                echo "ALTERADO<br>";
                            } else {
                                $licitacao = new Licitacao();
                                $licitacao->codigo_cidade = $this->codigoCidade;
                                $licitacao->link = trim($row->find("a", 0)->href);
                                $licitacao->licitacao = trim($row->find("a", 0)->plaintext);
                                $licitacao->objetivo = utf8_decode(utf8_encode(trim($row->find("td", 2)->plaintext)));
                                $date = DateTime::createFromFormat('d/m/Y', trim($row->find("td", 3)->plaintext));
                                $licitacao->data_abertura = $date->format('Y-m-d');
                                $licitacao->situacao = "concluida";
                                if(((int)$date->format("Y")) > (((int)date("Y")) - 2)) {
                                    $licitacao->save();
                                    $this->extrairLicitacaoDetalhes($licitacao);
                                    echo "CRIADO<br>";
                                } else {
                                    echo "Ano: " . (int)$date->format("Y");
                                    echo "<br>";
                                    echo "IGNORADO<br>";
                                }
                            }

                        } catch (Exception $e) {
                            throw $e;
                            echo "Erro: " . $e->getMessage();
                            echo "<br>";
                        }
                        if (trim($row->find("a", 0)->plaintext) === "" || in_array(trim($row->find("a", 0)->plaintext), $licCont)) {
                            echo "ENCERROU";
                            $contiue = false;
                        } else {
                            echo "CONTINUA";
                            $licCont[] = trim($row->find("a", 0)->plaintext);
                            $contiue = true;
                        }
                        echo "<br>";
                    }
                }
                $page++;
                sleep(1);
            } else {
                echo "ENCERROU - SEM RESULTADO";
                $contiue = false;
                echo "<br>";
            }
        }
    }

    public function extrairLicitacaoDetalhes($lc) {
        $url = self::URL_TCM . $lc->link;
        $html = HtmlDomParser::file_get_html($url);
        $this->extrairLicitacao($lc, $html);
    }

    public function extrairLicitacao($lc, $html) {
        //Detalhes
        echo "Detalhes<br>\n";
        $detalhes = array();
        foreach (explode("<br />", $html->find('.well', 0)) as $it) {
            $dt = explode(":", trim($it));
            if(count($dt) > 1) {
                if(trim(strip_tags($dt[0])) == "Modalidade") {
                    $mod = explode("|", trim(strip_tags($dt[1])));
                    $detalhes["Modalidade"] = trim($mod[0]);
                    $detalhes["Tipo"] = trim(strip_tags($dt[2]));
                } else {
                    $detalhes[trim(strip_tags($dt[0]))] = trim(strip_tags($dt[1]));
                }
            }
        }
        print_r($detalhes);
        $lc->exercicio = $detalhes["Exercício"];
        $lc->modalidade = $detalhes["Modalidade"];
        $lc->tipo = $detalhes["Tipo"];

        if(strripos($detalhes["Situação"], "Fechada") !== false) $lc->situacao = "fechada";
        if(strripos($detalhes["Situação"], "Aberta") !== false) $lc->situacao = "aberta";
        if(strripos($detalhes["Situação"], "Suspensa") !== false) $lc->situacao = "suspensa";
        if(strripos($detalhes["Situação"], "Concluida") !== false) $lc->situacao = "concluida";
        if(strripos($detalhes["Situação"], "Finalizada") !== false) $lc->situacao = "finalizada";

        echo "<br>\n";
        //Datas
        echo "Datas<br>\n";
        $datas = array();
        foreach (explode("</b>", $html->find('.well', 1)) as $it) {
            $dt = explode(":", trim(str_replace("|", "", $it)));
            if(count($dt) > 1) {
                if(strripos(trim(strip_tags($dt[0])), "Hora") !== false) {
                    $datas[trim(strip_tags($dt[0]))] = trim(strip_tags($dt[1])). ":" . trim(strip_tags($dt[2]) .  ":" . trim(strip_tags($dt[3])));
                } else {
                    $datas[trim(strip_tags($dt[0]))] = trim(strip_tags($dt[1]));
                }

            }
        }
        print_r($datas);
        $dateAviso = DateTime::createFromFormat('d-m-Y', $datas["Data da Publicação do Aviso"]);
        if(array_key_exists("Data da Publicação do Aviso", $datas)) $lc->data_aviso = $dateAviso->format('Y-m-d');
        if(array_key_exists("Hora da Abertura", $datas)) ;$lc->hora_abertura = $datas["Hora da Abertura"];
        if(array_key_exists("Local", $datas)) $lc->local = $datas["Local"];
        echo "<br>\n";

        //Dados Complementares
        echo "Dados Complementares<br>\n";
        $complemento = array();

        foreach (explode("<br />", $html->find('.well', 6)) as $it) {
            $comp = explode(":", trim(strip_tags($it)));
            if(count($comp) > 1) {
                if(trim(strip_tags($comp[0])) == "Nº do Processo Administrativo") {
                    $np = explode("|", trim($comp[1]));
                    $complemento["Nº do Processo Administrativo"] = trim($np[0]);
                    $complemento["Fundamentação Legal"] = trim($comp[2]);

                } else {
                    $complemento[trim(strip_tags($comp[0]))] = trim(strip_tags($comp[1]));
                }
            }
        }
        print_r($complemento);
        if(array_key_exists("Nº do Processo Administrativo", $complemento))$lc->processo_administrativo = $complemento["Nº do Processo Administrativo"];
        if(array_key_exists("Fundamentação Legal", $complemento))$lc->fundamentacao_legal = $complemento["Fundamentação Legal"];
        if(array_key_exists("Ordenador da Despesa", $complemento))$lc->ordenador_despesa = $complemento["Ordenador da Despesa"];
        if(array_key_exists("Pregoeiro/Presidente da Comissão", $complemento))$lc->pregoeiro_presidente_comissao = $complemento["Pregoeiro/Presidente da Comissão"];
        if(array_key_exists("Responsável pela Informação", $complemento))$lc->responsavel_informacao = $complemento["Responsável pela Informação"];
        if(array_key_exists("Responsável pelo Parecer Técnico Jurídico", $complemento))$lc->responsavel_parecer_tecnico_juridico = $complemento["Responsável pelo Parecer Técnico Jurídico"];
        if(array_key_exists("Responsável pela Adjudicação", $complemento))$lc->responsavel_adjudicacao = $complemento["Responsável pela Adjudicação"];
        if(array_key_exists("Responsável pela Homologação", $complemento))$lc->responsavel_homologacao = $complemento["Responsável pela Homologação"];

        //Verificar se é Camara municipal
        $isCamara = false;
        $stringsCamara = [
            "Camara Municipal",
            "CAMARA MUNICIPAL"
        ];
        foreach ($html->find('.well', 3)->find("ul", 0)->find("li") as $it) {
            foreach ($stringsCamara as $stCam) {
                if(strpos( $it->plaintext, $stCam) !== false) {
                    $isCamara = true;
                }
            }
        }
        if(!$isCamara) {
            try {
                $lc->save();
                echo "Salvando: " . $lc->licitacao;
                echo "<br>\n";
                $this->extrairPublicacao($lc, $html->find('.well', 2));
                $this->extrairOrgao($lc, $html->find('.well', 3));
                $this->extrairLicitante($lc, $html->find('.well', 4));
                $this->extrairObjeto($lc, $html->find('.well', 5));
            } catch (Exception $e) {
                throw $e;
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        } else {
            try {
                echo "Apagando: " . $lc->licitacao;
                echo "Motivo: Camara Municipal";
                $lc->delete();
                echo "<br>\n";
            } catch (Exception $e) {
                throw $e;
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
    }

    public function extrairPublicacao($lc, $html) {
        //Publicacao
        echo "Publicações<br>\n";
        $publicacoes = array();
        foreach ($html->find("ul", 0)->find("li") as $it) {
            $pb = explode("|", trim($it->plaintext));
            if(count($pb) > 2) {
                $pub = array();
                $pub["Categoria"] = trim($pb[0]);
                $esp = explode(":", trim($pb[1]));
                $pub["Especificação"] = trim($esp[1]);
                $dt = explode(":", trim($pb[2]));
                $pub["Data"] = trim($dt[1]);
                $publicacoes[] = $pub;
            }
        }
        print_r($publicacoes);
        foreach ($publicacoes as $pbl) {
            try {
                $publicacao = LicitacaoPublicacao::where("id_licitacao", "=", $lc->id)->where("especificacao", "=", $pbl["Especificação"])->first();
                if($publicacao) {
                    $publicacao->id_licitacao = $lc->id;
                    $publicacao->especificacao = $pbl["Especificação"];
                    $publicacao->categoria = $pbl["Categoria"];
                    $data = DateTime::createFromFormat('d-m-Y', $pbl["Data"]);
                    $publicacao->data = $data->format('Y-m-d');
                    $publicacao->save();
                } else {
                    $publicacao = new LicitacaoPublicacao();
                    $publicacao->id_licitacao = $lc->id;
                    $publicacao->especificacao = $pbl["Especificação"];
                    $publicacao->categoria = $pbl["Categoria"];
                    $data = DateTime::createFromFormat('d-m-Y', $pbl["Data"]);
                    $publicacao->data = $data->format('Y-m-d');
                    $publicacao->save();
                }
            } catch (Exception $e) {
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
        echo "<br>\n";
    }

    public function extrairOrgao($lc, $html) {
        //Publicacao
        echo "Orgãos<br>\n";
        $orgaos = array();

        foreach ($html->find("ul", 0)->find("li") as $it) {
            $orgaos[] = $it->plaintext;
        }
        print_r($orgaos);
        foreach ($orgaos as $og) {
            try {
                $orgao = LicitacaoOrgao::where("id_licitacao", "=", $lc->id)->where("orgao", "=", $og)->first();
                if(!$orgao) {
                    $orgao = new LicitacaoOrgao();
                    $orgao->id_licitacao = $lc->id;
                    $orgao->orgao = $og;
                    $orgao->save();
                }
            } catch (Exception $e) {
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
        echo "<br>\n";
    }

    public function extrairObjeto($lc, $html) {
        //Publicacao
        echo "Objeto<br>\n";
        $objetos = array();
        if($html->find("b", 0)->plaintext != "Objeto/Lotes/Itens") {
            echo "Não existe Objeto<br>\n";
            return;
        }
        if(!$html->find("ul", 0)){
            echo "Não existe Objeto<br>\n";
            return;
        }
        foreach ($html->find("ul", 0)->find("li") as $it) {
            $obArray = explode(":", trim($it->plaintext));
            array_shift($obArray);
            $ob = implode(":", $obArray);
            $objetos[] = $ob;
        }
        print_r($objetos);
        foreach ($objetos as $ob) {
            try {
                $objeto = LicitacaoObjeto::where("id_licitacao", "=", $lc->id)->where("objeto", "=", $ob)->first();
                if(!$objeto) {
                    $objeto = new LicitacaoObjeto();
                    $objeto->id_licitacao = $lc->id;
                    $objeto->objeto = $ob;
                    $objeto->save();
                }
            } catch (Exception $e) {
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
        echo "<br>\n";
    }

    public function extrairLicitante($lc, $html) {
        //Publicacao
        echo "Licitantes<br>\n";
        $licitantes = array();
        foreach ($html->find("ul", 0)->find("li") as $it) {
            if($it->class == "vencedor") {
                $lct = array();
                $lcArray = explode("|", trim($it->plaintext));
                print_r($lcArray);

                $lct["licitante"] = (explode(":", $lcArray[0]))[1];
                $lct["documento"] = (explode(":", $lcArray[1]))[1];
                $obArray = explode(":", $lcArray[2]);
                array_shift($obArray);
                $lct["objeto"] = implode(":", $obArray);
                $lct["valor"] = (int)str_replace(",", ".", str_replace(".", "", str_replace("R$ ", "",  (explode(":", $lcArray[count($lcArray) - 1]))[1])));
                $licitantes[] = $lct;
            }
        }
        print_r($licitantes);
        foreach ($licitantes as $ltn) {
            try {
                $licitante = LicitacaoLicitante::where("id_licitacao", "=", $lc->id)->where("licitante", "=", $ltn["licitante"])->first();
                if(!$licitante) {
                    $objeto = LicitacaoObjeto::where("id_licitacao", "=", $lc->id)->where("objeto", "=", $ltn["objeto"])->first();
                    if(!$objeto) {
                        $objeto = new LicitacaoObjeto();
                        $objeto->id_licitacao = $lc->id;
                        $objeto->objeto = $ltn["objeto"];
                        $objeto->save();
                    }
                    $licitante = new LicitacaoLicitante();
                    $licitante->id_licitacao = $lc->id;
                    $licitante->documento = $ltn["documento"];
                    $licitante->id_objeto = $objeto->id;
                    $licitante->licitante = $ltn["licitante"];
                    $licitante->valor = $ltn["valor"];
                    $licitante->save();                }
            } catch (Exception $e) {
                echo "Erro: " . $e->getMessage();
                echo "<br>";
            }
        }
        echo "<br>\n";
    }

    public function limparBase(){
        DB::table('licitacao')->truncate();
        DB::table('licitacao_publicacao')->truncate();
        DB::table('licitacao_orgao')->truncate();
        DB::table('licitacao_objeto')->truncate();
        DB::table('licitacao_licitante')->truncate();
        return "Limpeza realizada com sucesso!";
    }

    function win_to_utf8($str) {
        $f[]="\xc2\xac";  $t[]="\x80";
        $f[]="\xd9\xbe";  $t[]="\x81";
        $f[]="\xc0\x9a";  $t[]="\x82";
        $f[]="\xc6\x92";  $t[]="\x83";
        $f[]="\xc0\x9e";  $t[]="\x84";
        $f[]="\xc0\xa6";  $t[]="\x85";
        $f[]="\xc0\xa0";  $t[]="\x86";
        $f[]="\xc0\xa1";  $t[]="\x87";
        $f[]="\xcb\x86";  $t[]="\x88";
        $f[]="\xc0\xb0";  $t[]="\x89";
        $f[]="\xd9\xb9";  $t[]="\x8a";
        $f[]="\xc0\xb9";  $t[]="\x8b";
        $f[]="\xc5\x92";  $t[]="\x8c";
        $f[]="\xda\x86";  $t[]="\x8d";
        $f[]="\xda\x98";  $t[]="\x8e";
        $f[]="\xda\x88";  $t[]="\x8f";
        $f[]="\xda\xaf";  $t[]="\x90";
        $f[]="\xc0\x98";  $t[]="\x91";
        $f[]="\xc0\x99";  $t[]="\x92";
        $f[]="\xc0\x9c";  $t[]="\x93";
        $f[]="\xc0\x9d";  $t[]="\x94";
        $f[]="\xc0\xa2";  $t[]="\x95";
        $f[]="\xc0\x93";  $t[]="\x96";
        $f[]="\xc0\x94";  $t[]="\x97";
        $f[]="\xda\xa9";  $t[]="\x98";
        $f[]="\xc4\xa2";  $t[]="\x99";
        $f[]="\xda\x91";  $t[]="\x9a";
        $f[]="\xc0\xba";  $t[]="\x9b";
        $f[]="\xc5\x93";  $t[]="\x9c";
        $f[]="\xc0\x8c";  $t[]="\x9d";
        $f[]="\xc0\x8d";  $t[]="\x9e";
        $f[]="\xda\xba";  $t[]="\x9f";
        $f[]="\xd8\x8c";  $t[]="\xa1";
        $f[]="\xda\xbe";  $t[]="\xaa";
        $f[]="\xd8\x9b";  $t[]="\xba";
        $f[]="\xd8\x9f";  $t[]="\xbf";
        $f[]="\xdb\x81";  $t[]="\xc0";
        $f[]="\xd8\xa1";  $t[]="\xc1";
        $f[]="\xd8\xa2";  $t[]="\xc2";
        $f[]="\xd8\xa3";  $t[]="\xc3";
        $f[]="\xd8\xa4";  $t[]="\xc4";
        $f[]="\xd8\xa5";  $t[]="\xc5";
        $f[]="\xd8\xa6";  $t[]="\xc6";
        $f[]="\xd8\xa7";  $t[]="\xc7";
        $f[]="\xd8\xa8";  $t[]="\xc8";
        $f[]="\xd8\xa9";  $t[]="\xc9";
        $f[]="\xd8\xaa";  $t[]="\xca";
        $f[]="\xd8\xab";  $t[]="\xcb";
        $f[]="\xd8\xac";  $t[]="\xcc";
        $f[]="\xd8\xad";  $t[]="\xcd";
        $f[]="\xd8\xae";  $t[]="\xce";
        $f[]="\xd8\xaf";  $t[]="\xcf";
        $f[]="\xd8\xb0";  $t[]="\xd0";
        $f[]="\xd8\xb1";  $t[]="\xd1";
        $f[]="\xd8\xb2";  $t[]="\xd2";
        $f[]="\xd8\xb3";  $t[]="\xd3";
        $f[]="\xd8\xb4";  $t[]="\xd4";
        $f[]="\xd8\xb5";  $t[]="\xd5";
        $f[]="\xd8\xb6";  $t[]="\xd6";
        $f[]="\xd8\xb7";  $t[]="\xd8";
        $f[]="\xd8\xb8";  $t[]="\xd9";
        $f[]="\xd8\xb9";  $t[]="\xda";
        $f[]="\xd8\xba";  $t[]="\xdb";
        $f[]="\xd9\x80";  $t[]="\xdc";
        $f[]="\xd9\x81";  $t[]="\xdd";
        $f[]="\xd9\x82";  $t[]="\xde";
        $f[]="\xd9\x83";  $t[]="\xdf";
        $f[]="\xd9\x84";  $t[]="\xe1";
        $f[]="\xd9\x85";  $t[]="\xe3";
        $f[]="\xd9\x86";  $t[]="\xe4";
        $f[]="\xd9\x87";  $t[]="\xe5";
        $f[]="\xd9\x88";  $t[]="\xe6";
        $f[]="\xd9\x89";  $t[]="\xec";
        $f[]="\xd9\x8a";  $t[]="\xed";
        $f[]="\xd9\x8b";  $t[]="\xf0";
        $f[]="\xd9\x8c";  $t[]="\xf1";
        $f[]="\xd9\x8d";  $t[]="\xf2";
        $f[]="\xd9\x8e";  $t[]="\xf3";
        $f[]="\xd9\x8f";  $t[]="\xf5";
        $f[]="\xd9\x90";  $t[]="\xf6";
        $f[]="\xd9\x91";  $t[]="\xf8";
        $f[]="\xd9\x92";  $t[]="\xfa";
        $f[]="\xc0\x8e";  $t[]="\xfd";
        $f[]="\xc0\x8f";  $t[]="\xfe";
        $f[]="\xdb\x92";  $t[]="\xff";
        return str_replace($t, $f, $str);
    }
}