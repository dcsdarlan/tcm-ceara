<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 13/01/17
 * Time: 10:19
 */

namespace app\Classes\TCM;


abstract class TCMCeara {
    const URL_TCM = "http://www.tcm.ce.gov.br/";
    const URL_LICITACOES = self::URL_TCM . "licitacoes/index.php/licitacao";
    const URL_DISPENSAR_INEXIGIBILIDADES = self::URL_TCM . "licitacoes/index.php/dispensa_inexibilidade";
    const URL_REGISTRO_PRECO = self::URL_TCM . "licitacoes/index.php/registro_preco";
    public $codigoCidade = 0;

    function __construct($codigoCidade) {
        $this->codigoCidade = $codigoCidade;
    }

}