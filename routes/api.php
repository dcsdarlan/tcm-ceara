<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function(){
    Route::group(['prefix' => 'v1'], function(){
        Route::group(['prefix' => 'licitacoes'], function () {
            Route::get('/cidade/{cidade}/{x}/{y?}', [
                'as' => 'licitacao.detalhar',
                'uses' => 'LicitacaoController@detalhar'
            ]);
            Route::post('/filtrar/{codigo}', [
                'as' => 'licitacao.filtrar',
                'uses' => 'LicitacaoController@filtrar'
            ]);
            Route::get('/abertas/{codigo}', [
                'as' => 'licitacao.abertas',
                'uses' => 'LicitacaoController@abertas'
            ]);
            Route::get('/concluidas/{codigo}', [
                'as' => 'licitacao.abertas',
                'uses' => 'LicitacaoController@concluidas'
            ]);
        });
    });
});
