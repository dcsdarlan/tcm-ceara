<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function() {
    echo "TCM CEARA";
});

Route::group(['prefix' => 'extrair'], function () {
    Route::group(['prefix' => 'licitacao'], function () {
        Route::get('/cidade/{codigo}', [
            'as' => 'extrator.licitacao.cidade',
            'uses' => 'ExtratorLicitacaoController@extrair'
        ]);
        Route::get('/crateus', [
            'as' => 'extrator.licitacao.crateus',
            'uses' => 'ExtratorLicitacaoController@crateus'
        ]);
        Route::get('/ico', [
            'as' => 'extrator.licitacao.ico',
            'uses' => 'ExtratorLicitacaoController@ico'
        ]);
        Route::get('/eusebio', [
            'as' => 'extrator.licitacao.eusebio',
            'uses' => 'ExtratorLicitacaoController@eusebio'
        ]);
        Route::get('/maranguape', [
            'as' => 'extrator.licitacao.maranguape',
            'uses' => 'ExtratorLicitacaoController@maranguape'
        ]);
        Route::get('/pacajus', [
            'as' => 'extrator.licitacao.pacajus',
            'uses' => 'ExtratorLicitacaoController@pacajus'
        ]);
        Route::get('/pedrabranca', [
            'as' => 'extrator.licitacao.pedrabranca',
            'uses' => 'ExtratorLicitacaoController@pedrabranca'
        ]);
    });
    Route::group(['prefix' => 'dispensa'], function () {
        Route::get('/extrair/{codigo}', [
            'as' => 'extrator.dispensa.extrair',
            'uses' => 'ExtratorDispensaController@extrair'
        ]);
        Route::get('/crateus', [
            'as' => 'extrator.dispensa.crateus',
            'uses' => 'ExtratorDispensaController@crateus'
        ]);
        Route::get('/ico', [
            'as' => 'extrator.dispensa.ico',
            'uses' => 'ExtratorDispensaController@ico'
        ]);
        Route::get('/eusebio', [
            'as' => 'extrator.dispensa.eusebio',
            'uses' => 'ExtratorDispensaController@eusebio'
        ]);
        Route::get('/maranguape', [
            'as' => 'extrator.dispensa.maranguape',
            'uses' => 'ExtratorDispensaController@maranguape'
        ]);
        Route::get('/pacajus', [
            'as' => 'extrator.dispensa.pacajus',
            'uses' => 'ExtratorDispensaController@pacajus'
        ]);
        Route::get('/pedrabranca', [
            'as' => 'extrator.dispensa.pedrabranca',
            'uses' => 'ExtratorDispensaController@pedrabranca'
        ]);
    });
    Route::group(['prefix' => 'registropreco'], function () {
        Route::get('/extrair/{codigo}', [
            'as' => 'extrator.registropreco.extrair',
            'uses' => 'ExtratorRegistroPrecoController@extrair'
        ]);
        Route::get('/crateus', [
            'as' => 'extrator.registropreco.crateus',
            'uses' => 'ExtratorRegistroPrecoController@crateus'
        ]);
        Route::get('/ico', [
            'as' => 'extrator.registropreco.ico',
            'uses' => 'ExtratorRegistroPrecoController@ico'
        ]);
        Route::get('/eusebio', [
            'as' => 'extrator.registropreco.eusebio',
            'uses' => 'ExtratorRegistroPrecoController@eusebio'
        ]);
        Route::get('/maranguape', [
            'as' => 'extrator.registropreco.maranguape',
            'uses' => 'ExtratorRegistroPrecoController@maranguape'
        ]);
        Route::get('/pacajus', [
            'as' => 'extrator.registropreco.pacajus',
            'uses' => 'ExtratorRegistroPrecoController@pacajus'
        ]);
        Route::get('/pedrabranca', [
            'as' => 'extrator.registropreco.pedrabranca',
            'uses' => 'ExtratorRegistroPrecoController@pedrabranca'
        ]);
    });
});
