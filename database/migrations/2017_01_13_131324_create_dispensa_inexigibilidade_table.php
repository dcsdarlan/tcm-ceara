<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispensaInexigibilidadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispensa_inexigibilidade', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('dispensa_inexigibilidade', 100);
            $table->string('codigo_cidade');
            $table->date('data_abertura');
            $table->enum('situacao', array('aberta', 'concluida', 'fechada', 'suspensa', 'finalizada'));
            $table->text('objetivo', 65535)->nullable();
            $table->string('link', 200)->nullable();
            $table->integer('exercicio')->nullable();
            $table->string('processo_administrativo', 100)->nullable();
            $table->text('fundamentacao_legal', 65535)->nullable();
            $table->string('ordenador_despesa', 150)->nullable();
            $table->string('responsavel_dispensa', 100)->nullable();
            $table->string('responsavel_informacao', 100)->nullable();
            $table->string('tipo_responsavel_informacao', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dispensa_inexigibilidade');
    }
}
