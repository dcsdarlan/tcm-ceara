<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispensaInexigibilidadePublicacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispensa_inexigibilidade_publicacao', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('id_dispensa_inexigibilidade')->nullable();
            $table->string('categoria', 100);
            $table->string('especificacao', 250);
            $table->date('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dispensa_inexigibilidade_publicacao');
    }
}
