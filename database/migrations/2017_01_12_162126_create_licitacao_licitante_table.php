<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicitacaoLicitanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licitacao_licitante', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('id_licitacao')->nullable();
            $table->string('licitante', 200);
            $table->string('documento', 200);
            $table->integer('id_objeto')->nullable();
            $table->double('valor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('licitacao_licitante');
    }
}
