<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroPrecoPublicacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_preco_publicacao', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('id_registro_preco')->nullable();
            $table->string('categoria', 100);
            $table->string('especificacao', 250);
            $table->date('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registro_preco_publicacao');
    }
}
