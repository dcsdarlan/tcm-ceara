<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLicitacaoPublicacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('licitacao_publicacao', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_licitacao')->nullable();
			$table->string('categoria', 100);
			$table->string('especificacao', 250);
			$table->date('data');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('licitacao_publicacao');
	}

}
