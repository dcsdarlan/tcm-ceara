<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLicitacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('licitacao', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('licitacao', 100);
			$table->string('codigo_cidade');
			$table->text('objetivo', 65535)->nullable();
			$table->date('data_abertura');
			$table->string('link', 200)->nullable();
			$table->integer('exercicio')->nullable();
			$table->string('modalidade', 100)->nullable();
			$table->string('tipo', 100)->nullable();
			$table->enum('situacao', array('aberta', 'concluida', 'fechada', 'suspensa', 'finalizada'));
			$table->date('data_aviso')->nullable();
			$table->time('hora_abertura')->nullable();
			$table->string('local', 250)->nullable();
			$table->string('processo_administrativo', 100)->nullable();
			$table->text('fundamentacao_legal', 65535)->nullable();
			$table->string('ordenador_despesa', 150)->nullable();
			$table->string('pregoeiro_presidente_comissao', 100)->nullable();
			$table->string('responsavel_informacao', 100)->nullable();
			$table->string('responsavel_parecer_tecnico_juridico', 100)->nullable();
			$table->string('responsavel_adjudicacao', 100)->nullable();
			$table->string('responsavel_homologacao', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('licitacao');
	}

}
