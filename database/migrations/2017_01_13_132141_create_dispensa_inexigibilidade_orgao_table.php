<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispensaInexigibilidadeOrgaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispensa_inexigibilidade_orgao', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('id_dispensa_inexigibilidade')->nullable();
            $table->string('orgao', 200);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dispensa_inexigibilidade_orgao');
    }
}
