<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroPrecoFornecedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_preco_fornecedor', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('id_registro_preco')->nullable();
            $table->string('fornecedor', 200);
            $table->string('documento', 200);
            $table->integer('id_objeto')->nullable();
            $table->double('valor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registro_preco_fornecedor');
    }
}
