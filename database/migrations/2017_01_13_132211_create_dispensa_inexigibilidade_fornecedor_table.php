<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispensaInexigibilidadeFornecedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispensa_inexigibilidade_fornecedor', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('id_dispensa_inexigibilidade')->nullable();
            $table->string('fornecedor', 200);
            $table->string('documento', 200);
            $table->integer('id_objeto')->nullable();
            $table->double('valor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dispensa_inexigibilidade_fornecedor');
    }
}
